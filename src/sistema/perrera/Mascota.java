/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.perrera;

/**
 *
 * @author estudiantes
 */
public class Mascota {
   private String nombre;
   private String raza;
   private float edad;
   private float peso;
   private String tipo;

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRaza() {
        return raza;
    }

    public float getEdad() {
        return edad;
    }

    public float getPeso() {
        return peso;
    }

    public String getTipo() {
        return tipo;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
    
    
    
}
